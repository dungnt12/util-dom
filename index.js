(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["timelineModule"] = factory();
	else
		root["timelineModule"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "byId", function() { return byId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "byClassName", function() { return byClassName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "qs", function() { return qs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "qsa", function() { return qsa; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasClass", function() { return hasClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addClass", function() { return addClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeClass", function() { return removeClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toggleClass", function() { return toggleClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addListener", function() { return addListener; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeAllListeners", function() { return removeAllListeners; });
var _eventHandlers = {}; // somewhere global

var addListener = function addListener(node, event, handler) {
  var capture = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

  if (!(String(node) in _eventHandlers)) {
    // _eventHandlers stores references to nodes
    _eventHandlers[String(node)] = {};
  }

  if (!(event in _eventHandlers[String(node)])) {
    // each entry contains another entry for each event type
    _eventHandlers[String(node)][event] = [];
  } // capture reference


  _eventHandlers[String(node)][event].push([handler, capture]);

  node.addEventListener(event, handler, capture);
};

var removeAllListeners = function removeAllListeners(node, event) {
  if (String(node) in _eventHandlers) {
    var handlers = _eventHandlers[String(node)];

    if (event in handlers) {
      var eventHandlers = handlers[event];

      for (var i = eventHandlers.length; i--;) {
        var handler = eventHandlers[i];
        node.removeEventListener(String(event), handler[0], handler[1]);
      }
    }
  }
};
/**
 * Returns a reference to an element by its ID.
 *
 * #### Example:
 *
 * ```
 * import { byId } from 'helpers/dom';
 *
 * const content = byId('main-content');
 * ```
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById
 */


var byId = function byId(id) {
  return document.getElementById(id);
};
/**
 * Returns an array of all child elements which have all of the given class names
 *
 * #### Example:
 *
 * ```
 * import { byClassName } from 'helpers/dom';
 *
 * const listItems = byClassName('list__items');
 * ```
 * @param {Element|Document} [ctx=document] - Root element. `document` by default
 * @return {Array}
 */


var byClassName = function byClassName(className) {
  var ctx = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return ctx.getElementsByClassName(className);
};
/**
 * Returns the first element within the document that matches the specified group of selectors
 *
 * #### Example:
 *
 * ```
 * import { qs } from 'helpers/dom';
 *
 * const content = qs('#main-content');
 * ```
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector
 * @see https://stackoverflow.com/questions/43032004/queryselector-in-typescript
 */


var qs = function qs(selector) {
  var ctx = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return ctx.querySelector(selector);
};
/**
 * Returns a list of the elements within the document that match the specified group of selectors
 *
 * #### Example:
 *
 * ```
 * import { qsa } from 'tsumami';
 *
 * const listItems = qsa('.list .list-items');
 * ```
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll
 */


var qsa = function qsa(selector) {
  var ctx = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  return ctx.querySelectorAll(selector);
};
/**
 * Check class is exist on target element
 */


var hasClass = function hasClass(el, className) {
  if (el.classList) {
    return el.classList.contains(className);
  }

  return !!el.className.match(new RegExp("(\\s|^)".concat(className, "(\\s|$)")));
};
/**
 * Add class to element if that class is not exist
 */


var addClass = function addClass(el, className) {
  if (el.classList) {
    el.classList.add(className);
  } else if (!hasClass(el, className)) {
    el.className += " ".concat(className);
  }
};
/**
 * Remove class from element
 */


var removeClass = function removeClass(el, className) {
  if (el.classList) {
    el.classList.remove(className);
  } else if (hasClass(el, className)) {
    var reg = new RegExp("(\\s|^)".concat(className, "(\\s|$)"));
    el.className = el.className.replace(reg, ' ');
  }
};
/**
 * Toggle class
 */


var toggleClass = function toggleClass(el, className) {
  if (!hasClass(el, className)) {
    addClass(el, className);
  } else {
    removeClass(el, className);
  }
};



/***/ })
/******/ ]);
});