declare const addListener: (node: Element | Document, event: string, handler: any, capture?: boolean) => void;
declare const removeAllListeners: (node: Element | Document, event: string) => void;
/**
 * Returns a reference to an element by its ID.
 *
 * #### Example:
 *
 * ```
 * import { byId } from 'helpers/dom';
 *
 * const content = byId('main-content');
 * ```
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById
 */
declare const byId: (id: string) => HTMLElement | null;
/**
 * Returns an array of all child elements which have all of the given class names
 *
 * #### Example:
 *
 * ```
 * import { byClassName } from 'helpers/dom';
 *
 * const listItems = byClassName('list__items');
 * ```
 * @param {Element|Document} [ctx=document] - Root element. `document` by default
 * @return {Array}
 */
declare const byClassName: (className: string, ctx?: Element | Document) => HTMLCollection;
/**
 * Returns the first element within the document that matches the specified group of selectors
 *
 * #### Example:
 *
 * ```
 * import { qs } from 'helpers/dom';
 *
 * const content = qs('#main-content');
 * ```
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector
 * @see https://stackoverflow.com/questions/43032004/queryselector-in-typescript
 */
declare const qs: <T extends Element>(selector: string, ctx?: Document | T) => Element | T;
/**
 * Returns a list of the elements within the document that match the specified group of selectors
 *
 * #### Example:
 *
 * ```
 * import { qsa } from 'tsumami';
 *
 * const listItems = qsa('.list .list-items');
 * ```
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll
 */
declare const qsa: <T extends Element>(selector: string, ctx?: Document | T) => NodeListOf<T>;
/**
 * Check class is exist on target element
 */
declare const hasClass: (el: HTMLElement, className: string) => boolean;
/**
 * Add class to element if that class is not exist
 */
declare const addClass: (el: HTMLElement, className: string) => void;
/**
 * Remove class from element
 */
declare const removeClass: (el: HTMLElement, className: string) => void;
/**
 * Toggle class
 */
declare const toggleClass: (el: HTMLElement, className: string) => void;
export { byId, byClassName, qs, qsa, hasClass, addClass, removeClass, toggleClass, addListener, removeAllListeners };
